import React from 'react'
import PropTypes from 'prop-types'

import logo from '../images/logoBlue.png'

const Header = (props) => (
    <header id="header" style={props.timeout ? {display: 'none'} : {}}>
        <div className="logo">
            <img src={logo} />
        </div>
        <div className="content">
            <div className="inner">
                <h1>Solinr Hosting</h1>
                <p style={{ fontWeight: "bold" }}>Empowering businesses with cloud technology</p>
            </div>
        </div>
        <nav>
            <ul>
                {/* TODO:
                  <li><a href="javascript:;" onClick={() => {props.onOpenArticle('intro')}} style={{ fontWeight: "bold" }}>Company</a></li>
                  <li><a href="javascript:;" onClick={() => {props.onOpenArticle('work')}} style={{ fontWeight: "bold" }}>Tech</a></li>
                */}
                <li><a href="#" style={{ fontWeight: "bold" }}>WIP...</a></li>
                <li><a href="//docs.solinr.net" style={{ fontWeight: "bold" }}>Docs</a></li>
                <li><a href="//status.peccy.net" target="_blank" style={{ fontWeight: "bold" }}>Status</a></li>
                <li><a href="javascript:;" onClick={() => {alert("You are not authorised to login!")}} style={{ fontWeight: "bold" }}>Login</a></li>
            </ul>
        </nav>
    </header>
)

Header.propTypes = {
    onOpenArticle: PropTypes.func,
    timeout: PropTypes.bool
}

export default Header
