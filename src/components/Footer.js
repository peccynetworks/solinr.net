import React from 'react'
import PropTypes from 'prop-types'

const Footer = (props) => (
    <footer id="footer" style={props.timeout ? {display: 'none'} : {}}>
        <p className="copyright" style={{ fontWeight: "bold", fontSize: "0.75em" }}>&copy; <a href="//peccy.net" target="_blank">Peccy Networks</a> 2014-2018</p>
    </footer>
)

Footer.propTypes = {
    timeout: PropTypes.bool
}

export default Footer
